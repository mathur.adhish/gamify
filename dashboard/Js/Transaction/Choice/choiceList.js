$( document ).ready(function() {
    showReport(0);

    $(document).on('click','.editRecord',function() {
        editBooking(this);
    });
    
    $(document).on('click','.deleteRecord',function() {
        var elm = this;
        $.confirm({
             title: 'Confirm!',
             content: 'Are u sure want to delete ?',
             buttons: {
                confirm: function () {
                     deleteData(elm);
                },
                cancel: function () {
                }
            }
        });
     });
});

function search(){
    $('#currentPage').val(1);
    showReport(0);
}

function showReport(offset){

    $("#recordTable").empty().html('<img src="../../images/loading.gif"/>');
    $('#pagination-bar').empty();
     
    var data = "choiceName="+$('#choiceName').val()
                +"&limit="+$('#limit').val()
                +"&offset="+offset;
    
    $.ajax({
        type: "POST",
        timeout: 120000,
        url:  "../../TransactionAjax/choiceListAjax.jsp?",
        data:data,
        success: function(res){
            $('#recordTable').html(res);
            
            //**Pagination**/
            var limit = parseInt($('#limit').val());
            var count = parseInt($('#totalReords').val());
            var currentPage= parseInt($('#currentPage').val());
            
            var totalPagination=1;
            if(limit!==0 && limit<count){
                 totalPagination= Math.ceil(count/limit);
            }
            
            $('#pagination-bar').append('<ul id="pagination-no" class="pagination-sm"></ul>');
            $('#pagination-no').twbsPagination({
                totalPages: totalPagination,
                visiblePages: 5,
                hideOnlyOnePage:false,
                initiateStartPageClick:false,
                startPage: currentPage,

                next: 'Next',
                prev: 'Prev',
                onPageClick: function (event, page) {
                    $('#currentPage').val(page);
                    offset = (limit*(page-1));
                    showReport(offset);
                }
            });
        },
        error : function (err){
            console.log(err);
            $("#recordTable").html("Internal server error"+err.responseText);
        }
    });
}

function editBooking(elm){
    var w =800; var h=500;
    var left = (screen.width/2)-(w/2);
    var top = (screen.height/2)-(h/2);
    var id= jQuery.trim($(elm).attr('id').substring(8));
    var targetWin = window.open ("modifyChoice.jsp?id="+id, "Update choice", 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
}
   
function successReponse(message,type){
     $("#errorDiv").text(message);
     var limit = parseInt($('#limit').val());
     var currentPage= parseInt($('#currentPage').val());
     var  offset = (limit*(currentPage-1));
     showReport(offset);
 }  
 
function deleteData(elm){
                       
    var id= $.trim($(elm).attr('id').substring(8));
    $.ajax({
        type: "POST",
        timeout: 120000,
        url:  "../master.do?frm=deleteMasterList",
        data:{
            "id":id,
            "type":"3"
        },
        success: function(res){
            
            console.log(res);
            
            $("#errorDiv").text(res.respMsg);
            $("#row"+id).remove();
        },
        error : function (err){
            console.log(err);
            $("#recordTable").html("Internal server error"+err.responseText);
        }  
    });                 
}