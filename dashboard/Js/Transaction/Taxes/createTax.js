$( document ).ready(function() {
     $("#createCategory").validate({
        rules: {
            "taxName": {
                required: true,
            },
            "texPercentage": {
                required: true,
            }
        },
        messages: {
            "taxName": {
                required: "Tax name required"
            },
            "texPercentage": {
                required: "Tax percentage required"
            }
        },
        submitHandler: function (form) { // for demo
            alert('valid form submitted'); // for demo
            return false; // for demo
        }
    });
});
  
function saveForm(){
    if($('#createCategory').valid()){
         document.createCategory.submit();
    }
}
