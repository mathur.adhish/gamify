$( document ).ready(function() {
     $("#updateCategory").validate({
        rules: {
            "locationName": {
                required: true,
            }
        },
        messages: {
            "locationName": {
                required: "Location name required"
            }
        },
        submitHandler: function (form) { // for demo
            alert('valid form submitted'); // for demo
            return false; // for demo
        }
    });
});
  
function updateForm(){
    if($('#updateCategory').valid()){
         document.updateCategory.submit();
    }
}
 
   